<?php

namespace Drupal\module_hero\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Form;

/**
 * Our custom hero form.
 */
class HeroForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "module_hero_heroform";
  }

  /**
   * {@inheritdoc}
   */

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name'] = [
      '#type' => 'textfield',
        '#name' => 'name',
      '#title' => $this->t('Vardas'),
        '#required' => TRUE,

    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('El.paštas'),
        '#required' => TRUE,
    ];

      $form['image']= [
          '#type' => 'file',
          '#name' => 'image',
          '#title' => $this->t('Nuotrauka'),

      ];

    $form['submit'] =[
      '#type' => 'submit',
      '#value' => $this->t('Pateikti'),



    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    drupal_set_message(
      'Registarcija pateikta sekmingai'
    );
    drupal_set_message(

        $_POST['image']

    );


  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('name'))) {
      $form_state->setErrorByName('name', $this->t('Iveskite savo varda'));
    }
   /** if (empty($form_state->getValue('email'))) {
          $form_state->setErrorByName('email', $this->t('Iveskite savo varda'));
      }*/
      if (empty($form_state->getValue('email', FILTER_VALIDATE_EMAIL))) {
          $form_state->setErrorByName('email', $this->t('Iveskite savo varda'));
      }
  }

}
